# Welcome

**CaosDB is now LinkAhead!**

**This repository is no longer used. Please refer to https://gitlab.com/linkahead/linkahead**

CaosDB is a software toolbox for data management. Its focus lies on managing
data in dynamic environments of research and development where changes are too
frequent or projects are too small to bear the great cost of implementing a
traditional specialized but inflexible data management system. The research
data management system (RDMS) CaosDB uses a semantic data model that allows to
grow and change in those dynamic environments!

You can find an [online demo](https://demo.indiscale.com) of CaosDB at [https://demo.indiscale.com](https://demo.indiscale.com).  The collected
documentation is hosted at [https://docs.indiscale.com](https://docs.indiscale.com).

If you want to get your feet wet yourself, you should take a look at [our
recommended way to start/use the CaosDB server](https://gitlab.com/caosdb/caosdb-docker).

This is the CaosDB meta repository. It contains general documents such as our [Code of Conduct](CODE_OF_CONDUCT.md)
and general guidelines which apply to all CaosDB subprojects.

## Useful links ##

- Documentation: https://docs.indiscale.com
- Chat: [#caosdb:matrix.org](https://matrix.to/#/!unwwlTfOznjEnMMXxf:matrix.org)

## Documents ##

- [Code of Conduct](CODE_OF_CONDUCT.md) We want everyone to feel safe and respected in the CaosDB community.  Our
  code of conduct is an effort to archieve this goal.
- [Our contributors](HUMANS.md) These people contributed to the CaosDB project.
- [Release Guidelines](RELEASE_GUIDELINES.md) What to do when releasing a new version of a subproject.  The CaosDB
  subproject often have more specific additional guidelines.
- [Review Guidelines](REVIEW_GUIDELINES.md) for satisfying and effective code reviews.

## CaosDB subprojects ##

### Good starting points ###

- [Python client library](https://gitlab.com/caosdb/caosdb-pylib) contains the Python client. Most users want to start here. You can for
  example access the demo server with the client.
  - [Advanced Python tools](https://gitlab.com/caosdb/caosdb-advanced-user-tools) that go beyond a simple use of the Python client. Especially, this
    includes the crawler for automated data integration.
- [caosdb-server](https://gitlab.com/caosdb/caosdb-server) contains the java sources of the server software. This is was you need to run
  your own CaosDB instance.
- [caosdb-webui](https://gitlab.com/caosdb/caosdb-webui) contains the web client (is typically installed with caosdb-server and is a
  submodule of it).

### More APIs and client libraries ###

- [CaosDB Protobuf API](https://gitlab.com/caosdb/caosdb-proto) the protobuf files which make up the gRPC API of CaosDB.
- [C++ library](https://gitlab.com/caosdb/caosdb-cpplib) is the C++ core library for accessing the gRPC API.
- [Julia client library](https://gitlab.com/caosdb/caosdb-julialib) for interaction with CaosDB through Julia.  Uses the C++ library.
- [Octave client library](https://gitlab.com/caosdb/caosdb-octavelib) for interaction with CaosDB through Octave/Matlab.  Uses the C++
  library.

### Miscellaneous ###

- [Docker](https://gitlab.com/caosdb/caosdb-docker) for running CaosDB and other components in a Docker environment.
- [Crawler](https://gitlab.com/caosdb/caosdb-crawler) is the new crawler system for automated data integration, but still WIP (as of
  2022-09).
- [caosdb-mysqlbackend](https://gitlab.com/caosdb/caosdb-mysqlbackend) contains sources for setting up and interacting with the MySQL/MariaDB
  backend. You need it for running the server.
- [Python integration tests](https://gitlab.com/caosdb/caosdb-pyinttest) the integration tests for the system with the highest coverage.
- [Julia integration tests](https://gitlab.com/caosdb/caosdb-juliainttest) integration tests for Julia.

# Maintainers #

* Timm Fitschen (Lead)
* Daniel Hornung
* Alexander Schlemmer
* Henrik tom Wörden

# License #

Generally, the CaosDB project and all files in the official repositories are licensed under the GNU
AGPLv3 [License](LICENSE.md).  However, always check the subprojects' `LICENSE.md` files for the
license which applies in the particular case.
