Thank you very much to all contributers—[past, present](HUMANS.md), and
prospective ones.

# Code of Conduct

By participating, you are expected to uphold our [Code of
Conduct](CODE_OF_CONDUCT.md).

# How to Contribute

You found a bug, have a question, or want to request a feature? Please file a
Gitlab issue in the repository that you deem most fitting (if you're not sure,
you can also use this one).

## Contributing with a merge request

If you want to contribute to any of our repositories with code or documentation,
you are welcome to do so via a Gitlab merge request (MR).

In order to assure code quality there are a few things to be considered before
your contribution is merged. Apart from an extensive **code review**, automated
tests will check your code. [IndiScale GmbH](https://www.indiscale.com/)
provides servers that run the pipelines with automated unit and integration
testing run on [IndiScale's own Gitlab
instance](https://gitlab.indiscale.com/caosdb/src). MRs will only be merged
after a positive review **and** after the automated testing pipelines on that
instance have succeeded.

### Open a MR

In order to contribute your own code or documentation, you first need to fork
the repository to which you want to make a contribution. You then implement your
contribution in that fork (remember to add unit and integration tests where
applicable). Once you think your contribution can be merged, you open a MR from
your fork into the original CaosDB repository which can then be reviewed by
others. Check the [guidelines](REVIEW_GUIDELINES.md) to see how to best describe
your MR and for the general review process.

During the review process, a copy of your suggestion will be made on
gitlab.indiscale.com where your contribution is checked against the automated
testing pipelines. Your contribution will be merged only after a positive code
review and if all automated tests are successful.

### Review a MR

Apart from contributing with your own code suggestions, you may also contribute
by reviewing an open MR. If you want to do that, select the MR that you want to
review and contact us, i.e., the maintainers of the corresponding repository
s.th. we can provide you with the necessary permissions. You can then start the
code review (read carefully the [guidelines](REVIEW_GUIDELINES.md) for the
review process). Once you are done with the review and you think the
contribution is ready to be merged, you **approve** the MR. A member of CaosDB's
maintainer team will subsequently merge the MR only after the test pipelines
have been successful.
